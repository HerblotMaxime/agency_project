<?php

namespace App\Entity;

use App\Repository\PossessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PossessionRepository::class)]
class Possession
{
    const STATUS_CREATED = 1;
    const STATUS_AVAILABLE = 2;
    const STATUS_NOT_AVAILABLE = 3;
    
    const SERVICE_TYPE_LOCATION = 1;


    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[ORM\ManyToOne(inversedBy: 'possessions')]
    private ?User $agent = null;

    #[ORM\Column]
    private ?string $type = null;

    #[ORM\Column]
    private ?float $price = null;

    #[ORM\Column]
    private ?int $status = null;

    #[ORM\OneToMany(mappedBy: 'possession', targetEntity: Picture::class)]
    private Collection $pictures;

    #[ORM\OneToMany(mappedBy: 'possession', targetEntity: PossessionInformation::class)]
    private Collection $possessionInformation;

    #[ORM\OneToMany(mappedBy: 'possession', targetEntity: Appointment::class)]
    private Collection $appointments;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column]
    private ?int $surface = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $creating_date = null;

    #[ORM\Column]
    private ?int $type_service = null;

    public function __construct()
    {
        $this->pictures = new ArrayCollection();
        $this->possessionInformation = new ArrayCollection();
        $this->appointments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAgent(): ?User
    {
        return $this->agent;
    }

    public function setAgent(?User $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, Picture>
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures->add($picture);
            $picture->setPossession($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getPossession() === $this) {
                $picture->setPossession(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PossessionInformation>
     */
    public function getPossessionInformation(): Collection
    {
        return $this->possessionInformation;
    }

    public function addPossessionInformation(PossessionInformation $possessionInformation): self
    {
        if (!$this->possessionInformation->contains($possessionInformation)) {
            $this->possessionInformation->add($possessionInformation);
            $possessionInformation->setPossession($this);
        }

        return $this;
    }

    public function removePossessionInformation(PossessionInformation $possessionInformation): self
    {
        if ($this->possessionInformation->removeElement($possessionInformation)) {
            // set the owning side to null (unless already changed)
            if ($possessionInformation->getPossession() === $this) {
                $possessionInformation->setPossession(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Appointment>
     */
    public function getAppointments(): Collection
    {
        return $this->appointments;
    }

    public function addAppointment(Appointment $appointment): self
    {
        if (!$this->appointments->contains($appointment)) {
            $this->appointments->add($appointment);
            $appointment->setPossession($this);
        }

        return $this;
    }

    public function removeAppointment(Appointment $appointment): self
    {
        if ($this->appointments->removeElement($appointment)) {
            // set the owning side to null (unless already changed)
            if ($appointment->getPossession() === $this) {
                $appointment->setPossession(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSurface(): ?int
    {
        return $this->surface;
    }

    public function setSurface(int $surface): self
    {
        $this->surface = $surface;

        return $this;
    }

    public function getCreatingDate(): ?\DateTimeInterface
    {
        return $this->creating_date;
    }

    public function setCreatingDate(\DateTimeInterface $creating_date): self
    {
        $this->creating_date = $creating_date;

        return $this;
    }

    public function getTypeService(): ?int
    {
        return $this->type_service;
    }

    public function setTypeService(int $type_service): self
    {
        $this->type_service = $type_service;

        return $this;
    }
}
