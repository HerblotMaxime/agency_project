<?php

namespace App\Form\Possession;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class Edit extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class,[
                'attr' => ['class' => 'form-control'],
                'label' => 'Titre de l\'annonce'
            ])
            ->add('type', TextType::class,[
                'attr' => ['class' => 'form-control'],
                'label' => 'Type de bien'
            ])
            ->add('price', IntegerType::class,[
                'attr' => ['class' => 'form-control'],
                'label' => 'Prix du bien'
            ])
            ->add('description', TextareaType::class,[
                'attr' => ['class' => 'form-control'],
                'label' => 'Description du bien'
            ])
            ->add('surface', NumberType::class,[
                'attr' => ['class' => 'form-control'],
                'label' => 'Surface habitable'
            ])
            ->add('Save', SubmitType::class,[
                'label' => 'Enregistrer l \'annonce'
            ]);
    }
}
