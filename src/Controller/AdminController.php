<?php

namespace App\Controller;

use App\Entity\Possession;
use App\Repository\PossessionRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'app_admin')]
    public function dashboard(ManagerRegistry $doctrine): Response
    {

        $currentUser = $this->getUser();
        $userId = $currentUser->getId();

        $em = $doctrine->getManager();
        $possessionRepo = $em->getRepository(Possession::class);
        if (in_array('ROLE_ADMIN',$currentUser->getRoles())){
            $data['possessions'] = $possessionRepo->findAll();
            $data['appointements'] = $currentUser->getAppointment();
        } else {
            $data['possessions'] = $possessionRepo->findBy(['agent' => $userId]);
            $data['appointements'] = $currentUser->getAppointment();
        }

        return $this->render('admin/dashboard.html.twig', $data);
    }
}
