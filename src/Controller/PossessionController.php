<?php

namespace App\Controller;

use App\Entity\Appointment;
use App\Entity\Possession;
use App\Entity\User;
use App\Form\Possession\CreateEdit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PossessionController extends AbstractController
{
    #[Route('/possession', name: 'app_possession')]
    public function index(): Response
    {
        return $this->render('possession/index.html.twig', [
            'controller_name' => 'PossessionController',
        ]);
    }

    #[Route('/possession/create', name: 'app_possession_create')]
    public function create(Request $request, ManagerRegistry $doctrine): Response
    {
        $data = [];

        $form = $this->createForm(CreateEdit::class, []);
        $data['form'] = $form->createView();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $possessionData = $form->getData();
            $user = $this->getUser();
            $possession = new Possession();


            $possession->setType($possessionData['type'])
                ->setPrice($possessionData['price'])
                ->setDescription($possessionData['description'])
                ->setTitle($possessionData['title'])
                ->setSurface($possessionData['surface'])
                ->setStatus(Possession::STATUS_CREATED)
                ->setCreatingDate(date_create())
                ->setTypeService(Possession::SERVICE_TYPE_LOCATION)
                ->setAgent($user);

            $em = $doctrine->getManager();
            $em->persist($possession);
            $em->flush();
            return $this->redirectToRoute('app_admin');
        }


        return $this->render('possession/createEdit.html.twig', $data);
    }

    #[Route('/possession/edit/{id}', name: 'app_possession_edit')]
    public function edit($id, ManagerRegistry $doctrine, Request $request): Response
    {
        $formData = [];
        $em = $doctrine->getManager();
        $possessionRepo = $em->getRepository(Possession::class);
        $possession = $possessionRepo->find($id);

        $formData = [
            'title' => $possession->getTitle(),
            'type' => $possession->getType(),
            'price' => $possession->getPrice(),
            'description' => $possession->getDescription(),
            'surface' => $possession->getSurface()
        ];
        $form = $this->createForm(CreateEdit::class, $formData);
        $data['form'] = $form->createView();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $possessionData = $form->getData();
            $possession->setType($possessionData['type'])
                ->setPrice($possessionData['price'])
                ->setDescription($possessionData['description'])
                ->setTitle($possessionData['title'])
                ->setSurface($possessionData['surface']);
            $em->persist($possession);
            $em->flush();
            return $this->redirectToRoute('app_admin');
        }


        return $this->render('possession/createEdit.html.twig', $data);
    }


    #[Route('/possession/delete/{id}', name: 'app_possession_delete', methods: ['GET'])]
    public function delete($id, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();

        $possessionRepo = $em->getRepository(Possession::class);
        $possession = $possessionRepo->find($id);
        $em->remove($possession);
        $em->flush();

        $this->addFlash('notice', 'La suppression a été réussi');
        return $this->redirectToRoute('app_admin');
    }

    #[Route('/possession/status/edit', name: 'app_possession_edit_status', methods: ['POST'])]
    public function changeStatus(Request $request, ManagerRegistry $doctrine): JsonResponse
    {

        $em = $doctrine->getManager();
        $data = [];
        $id = $request->get('id');
        $status = $request->get('status');
        $possessionRepo = $em->getRepository(Possession::class);

        $possession = $possessionRepo->find($id);
        $possession->setStatus($status);
        $em->persist($possession);
        $em->flush();

        return new JsonResponse();
    }

    #[Route('/delAppointement', name: 'app_del_appointement', methods: ['POST'])]
    public function delAppointement(Request $request, ManagerRegistry $doctrine): JsonResponse
    {

        $em = $doctrine->getManager();
        $data = [];
        $id = $request->get('id');
        $AppointementRepo = $em->getRepository(Appointment::class);

        $Appointement = $AppointementRepo->find($id);
        $em->remove($Appointement);
        $em->flush();

        return new JsonResponse();
    }


    #[Route('/possession/appointment/save', name: 'app_save_appointment', methods: ['POST'])]
    public function saveAppointment(Request $request, ManagerRegistry $doctrine): JsonResponse
    {

        $em = $doctrine->getManager();
        $id = $request->get('id');
        $date = $request->get('date');
        $agentId = $request->get('agentId');
        
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($agentId);

        $possessionRepo = $em->getRepository(Possession::class);
        $possession = $possessionRepo->find($id);

        $datetime = \DateTime::createFromFormat("Y-m-d\TH:i",$date);
        $appointment = new Appointment();
        $appointment->setDate($datetime);
        $appointment->setOrganiser($user);
        $appointment->setPossession($possession);
        $em->persist($appointment);
        $em->flush();

        return new JsonResponse();
    }


}
