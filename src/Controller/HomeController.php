<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Possession;

use App\Repository\PossessionRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(PossessionRepository $possessionRepository, ManagerRegistry $doctrine): Response
    {

        $qb = $possessionRepository->createQueryBuilder("p")
            ->orderBy('p.creating_date', 'DESC')
            ->setMaxResults(5);
        $articles = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'articles' => $articles,
        ]);
    }

    #[Route('/generate/superadmin', name: 'app_generate_user')]
    public function generateUser(UserPasswordHasherInterface $passwordHasher, ManagerRegistry $doctrine): Response
    {
        $user = new User();
        $plaintextPassword = "superadmin";

        // hash the password (based on the security.yaml config for the $user class)
        $hashedPassword = $passwordHasher->hashPassword(
            $user,
            $plaintextPassword
        );
        $user->setEmail('superadmin@agency.com');
        $user->setPassword($hashedPassword);
        $user->setRoles(['ROLE_ADMIN']);
        $entityManager = $doctrine->getManager();

        $entityManager->persist($user);
        $entityManager->flush();
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    #[Route('/articles/{page}', name: 'app_articles', defaults: ['page' => 1])]
    public function getArticles(PossessionRepository $possessionRepository, $page)
    {
        $qb = $possessionRepository->createQueryBuilder("p")
            ->where('p.status != :status')
            ->orderBy('p.creating_date', 'DESC');
        $qb->setParameter('status', Possession::STATUS_NOT_AVAILABLE);
        $articles = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
        $nbArticle = count($articles);
        $nbPage = ceil($nbArticle / 10);

        if ($page > 0) {
            $qb = $possessionRepository->createQueryBuilder("p")
                ->where('p.status != :status')
                ->setFirstResult(($page-1) * 10)
                ->setMaxResults(10)
                ->orderBy('p.creating_date', 'DESC');
            $qb->setParameter('status', Possession::STATUS_NOT_AVAILABLE);
            $articles = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
        }

        return $this->render('home/index.html.twig', [
            'nbArticle' => $nbArticle,
            'articles' => $articles,
            'nbPage' => $nbPage,
            'currentPage' => $page,
        ]);
    }

    #[Route('/product/{id}', name: 'app_article')]
    public function show(PossessionRepository $possessionRepository, int $id)
    {
        $article = $possessionRepository->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'No aticle found for id ' . $id
            );
        }


        return $this->render('article/index.html.twig', [
            'article' => $article,
        ]);
    }
}
